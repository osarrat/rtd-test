Documentation du Back Office de la plateforme de covoiturage

|image0|

1. .. rubric:: Les acteurs de la plateforme de covoiturage
      :name: les-acteurs-de-la-plateforme-de-covoiturage

   1. .. rubric:: L'internaute
         :name: linternaute

Toute personne qui naviguant sur Internet visite la page d'accueil de la
plateforme. Elle peut consulter la liste des événements et des
communautés.

Elle peut proposer un trajet, mais alors elle doit s'inscrire, elle
devient lors un covoitureur.

Le covoitureur
--------------

Le covoitureur est un internaute qui a créé un compte sur la plate-forme
soit sur le portail soit en téléchargeant l'App.

1. .. rubric:: Le donneur d'ordre
      :name: le-donneur-dordre

2. .. rubric:: L'administrateur
      :name: ladministrateur

Le client/donneur d’ordre est autonome pour réaliser la plupart des
actions de configuration de la plateforme, mais certaines actions
demandent une intervention de l’équipe technique.

On désigne administrateur applicatif (AdminApp) le rôle des personnes
chargées de l’administration applicative, ce sont a priori des
représentants du donneur d'ordre et l’on désigne par AdminTech l’équipe
technique qui vient en support des demandes pour lesquelles AdminApp
n’est pas autonome.

Le référent
-----------

La notion de référent est associée à celle de communauté. Un référent
est l'animateur d'une communauté, il reçoit les notifications lorsqu’un
covoitureur demande à rejoindre une communauté privée.

1. .. rubric:: Présentation des concepts
      :name: présentation-des-concepts

   1. .. rubric:: Les communautés
         :name: les-communautés

Définition et caractéristiques d’une communauté d’usagers (groupe ou
sous groupe)

Notion et types de communautés
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Une communauté, est un groupe de personnes qui partagent quelque chose
en commun soit un événement, un territoire, un intérêt commun (groupe
d’individus, organisation, association, territoire).

Elle peut-être :

-  une ​ communauté publique ​ : toute personne ayant accès au site peut
   en devenir membre et voir les trajets associés ou

-  une ​ communauté privée ​ : l’adhésion est restreinte et modérée par
   un référent (validation manuelle du référent, ou code d’accès fournis
   par le référent, ou validation avec le domaine de l’email ou par une
   liste blanche d’utilisateurs) et les trajets associés ne sont
   visibles que par ses membres. Le plus souvent les communauté
   publiques sont des communautés dédiées aux trajets domicile-travail
   et les communautés privées sont plutôt dédiées aux trajets
   professionnels afin de garder ces trajets confidentiels et visibles
   que des membres de la communauté privée (souvent les employés). Dans
   d’autres cas d’usages (par exemple, dans le cas d’une entreprise
   multi-sites), il peut-être ajoutées d’autres communautés par
   l’administrateur. Visibilités des trajets selon la communauté Le fait
   de regrouper les utilisateurs dans des communautés leur permet :

-  De publier des trajets avec un “​ tag​ ” indiquant à quelle(s)
   communauté(s) ils appartiennent

-  De choisir entre :

   -  une ​ publication publique de leur trajet

   -  une ​ publication privée de leur trajet​ , c’est à dire de rendre
      visible leur trajet qu’aux membres d’une de leurs communautés
      privées.

-  De ​ gérer la visibilité de leur numéro de téléphone​ :

   -  Visible dans l'annonce dès le départ ou seulement après qu’un
      autre internaute ait décidé de faire équipe avec lui via la
      messagerie

   -  Dans le cas d’un trajet public :

      -  Soit visible dès la publication de l'annonce, par tous les
         inscrits

      -  Soit visible dès la publication de l'annonce, seulement par les
         adhérents de ma(mes) communauté(s)

      -  Soit, visible seulement après acceptation du covoiturage,
         uniquement par les participants

   -  Dans le cas d’un trajet privé :

      -  Soit visible dès la publication de l'annonce, seulement par les
         adhérents de ma communauté privée

      -  Soit visible seulement après acceptation du covoiturage avec un
         adhérent de ma communauté privée

Les points relais
-----------------

Les points relais (points de rendez-vous sur le portail) servent à
nommer un lieu et pouvoir le rappeler comme origine ou une destination
d’un trajet.

Ils ressemblent aux adresses que l’utilisateur peut créer dans son
compte, mais les points relais sont visibles par tout le monde.

Il y a deux manières de créer un point relais,

1. le premier se trouve dans le portail un bouton permet d’enregistrer
   une origine ou une destination en tant que point relais,

2. le second se trouve dans l’interface d’administration. On peut
   associer une image à un point relais.

Les communes
------------

Permet d'enregistrer les communes pour le référencement.

Les événements
--------------

Les événements culturels et sportifs sont générateurs de déplacements
massifs en un point unique sur un laps de temps très court.

Ils peuvent occasionner des congestions et une saturation des capacités
de stationnement sur un point du territoire.

Il y a donc un intérêt fort à augmenter le taux d'occupation des
véhicules en invitant les participants à covoiturer.

La plateforme de covoiturage propose pour cela un moyen d'annoncer les
événements sur la plateforme avec un lien direct pour créer une annonce
ayant le lieu de cet événement comme destination.

Un événement peut être créé :

-  par l'administrateur

-  pour tout abonné de la plateforme. L'administrateur dispose d'une
   fonction de modération sur les événements proposés pas les
   covoitureurs.

   1. .. rubric:: Les articles
         :name: les-articles

Les articles sont des textes qui sont accessible par le bouton « Aide »
de la page d’accueil du site.

La plateforme est fournies avec le texte initial de certaines
rubriques :

-  Assurance

-  Charte

-  Cookies

-  Fiscalité

-  Mentions légales

-  FAQ

L’administrateur peut modifier ces textes et ajouter d’autres articles.

1. .. rubric:: L'administration de la plateforme
      :name: ladministration-de-la-plateforme

   1. .. rubric:: Introduction
         :name: introduction

Il faut distinguer des activités qui peuvent être faites par le client
de manière autonome de celles qui nécessitent une intervention de
l'administrateur technique de la plateforme.

On désigne administrateur applicatif (AdminApp) le rôle des personnes
chargées de l’administration applicative, ce sont a priori des
représentants du donneur d'ordre.

On désigne Administrateur Technique (AdminTech) le rôle des
représentants du fournisseur qui effectuent les tâches techniques sur
demande de AdminApp. AdminApp dispose d'un compte dans le back office
d'administration de la plateforme et d'un compte dans l'outil de
ticketing de maintenance.

Les demandes d'intervention sont soumises par des tickets dans cet
outil.

Les droits de AdminApp sont gérés par module.

Pour chaque module on peut définir les droits CRUD (Create, Read,
Update, Delete)

En date du 31 mars 2018 la liste des modules installés est la suivante :

+-----------------+----------+--------+----------+----------+
| Module          | Create   | Read   | Update   | Delete   |
+=================+==========+========+==========+==========+
| Points relais   |          |        |          |          |
+-----------------+----------+--------+----------+----------+
| Evénements      |          |        |          |          |
+-----------------+----------+--------+----------+----------+
| Communautés     | X        |        |          |          |
+-----------------+----------+--------+----------+----------+
| Articles        |          |        |          |          |
+-----------------+----------+--------+----------+----------+
| Statistiques    | X        |        | X        | X        |
+-----------------+----------+--------+----------+----------+

Liste des fonctions d'administration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Objets du domaine

   -  Administration des `*points
      relais* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_points_relais>`__

   -  Administration des
      `*communes* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_communes>`__

   -  Administration des
      `*communautés* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_communaut.C3.A9s>`__

   -  Administration des articles

   -  Gestion des événements

      -  Création d'événement dans le back office

      -  Modération des événements créés par les covoitureurs

      -  Création d'événements par flux RSS

-  Animation du site

   -  emailing

   -  news letter

-  Supervision et reporting

   -  Reporting sur le portail Jasper

   -  Fréquentation du site avec Google Analytics

   1. .. rubric:: Le back office pour l'administrateur applicatif
         :name: le-back-office-pour-ladministrateur-applicatif

Fonctions qui sont accessibles à l'AdminApp :

Administration des Objets du domaine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Administration des `*points
   relais* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_points_relais>`__

-  Administration des
   `*communes* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_communes>`__

-  Administration des
   `*communautés* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_communaut.C3.A9s>`__

-  Administration des
   `*articles* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_articles>`__

-  Gestion des
   `*événements* <http://62.210.136.66/mediawiki/index.php/Présentation_des_concepts#Les_.C3.A9v.C3.A9nements>`__

   -  Création d'événement

   -  Modération des événements créés par les covoitureurs

   1. .. rubric:: Animation du site
         :name: animation-du-site

Les actions de ce groupe nécessitent une intervention de AdminTech.

Supervision et reporting
~~~~~~~~~~~~~~~~~~~~~~~~

-  Reporting sur le portail Jasper

-  Fréquentation du site avec Google Analytics

L'administration technique
--------------------------

Liste des fonctions qui nécessitent une intervention de AdminTech :

1. .. rubric:: Animation du site
      :name: animation-du-site-1

   #. .. rubric:: emailing
         :name: emailing

Texte et image à fournir à AdminTech. La prestation comprend deux envois
par an.

Flux RSS des événements
~~~~~~~~~~~~~~~~~~~~~~~

La mise en place de la création d’événements par flux RSS est du domaine
de l’administration technique.

AdminTech et AdminApp définissent le contenu du flux, AdminTech
implémente son traitement.

Dans le cadre du projet, il est prévu de créer un flux RSS avec le site
de la région, il pourrait y avoir d'autres collectivités intéressées.

.. |image0| image:: rst/media/media/image1.png
   :width: 7.01875in
   :height: 3.06250in
